# Automatic Generation of Programming Exercises in Jupyter

Welcome to the main repository, which acts as a hub for the two central parts of the implementation.

Firstly, a generator for Python programming exercises was implemented. This editor can be found in the branch "task_generator".
Secondly, to provide a way of easily accessing this generator, a JupyterLab extension was implemented, which can be found in the branch "jupyterlab_extension".

Both branches provide setup information and more detail in their own ReadMe files.
